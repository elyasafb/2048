from random import randint

import numpy as np

from BaseAI_3 import BaseAI
from Grid_3 import Grid

UP = 0
DOWN = 1
LEFT = 2
RIGHT = 3
move_score = {UP: 0, DOWN: 100, LEFT: 10, RIGHT: 50}

PLACE_FACTOR = [[0, 0, 1, 3],
                [0, 1, 3, 5],
                [1, 3, 5, 15],
                [3, 5, 15, 30]]

PLAYER = 'player'
RIVAL = 'rival'


class PlayerAI(BaseAI):
    def getMove(self, grid: Grid):
        next_grid, val = self.minimax(grid)

        for direction in grid.getAvailableMoves():
            copied_grid = grid.clone()
            copied_grid.move(direction)
            if copied_grid.map == next_grid.map:
                return direction

    def minimax(self, grid: Grid, depth=4, alpha=float('-inf'), beta=float('inf'), player=PLAYER):
        if depth == 0 or not grid.getAvailableMoves():
            return None, self.eval(grid)

        if player == PLAYER:
            max_eval = float('-inf')
            max_child = None
            for child in self.get_grid_player_children(grid):
                _, new_eval = self.minimax(child, depth - 1, alpha, beta, RIVAL)
                max_eval, max_child = (new_eval, child) if new_eval > max_eval else (max_eval, max_child)
                alpha = max(alpha, max_eval)
                if beta <= alpha:
                    break
            return max_child, max_eval
        elif player == RIVAL:
            min_val = float('inf')
            min_child = None
            for child in self.get_grid_rival_children(grid):
                _, new_eval = self.minimax(child, depth - 1, alpha, beta, PLAYER)
                min_val, min_child = (new_eval, child) if new_eval < min_val else (min_val, min_child)
                beta = min(beta, min_val)
                if beta <= alpha:
                    break
            return min_child, min_val

    def eval(self, grid):
        return self.evalFun(grid)

    def get_grid_player_children(self, grid):
        children = []
        for direction in grid.getAvailableMoves():
            new_grid = grid.clone()
            new_grid.move(direction)
            children.append(new_grid)
        children_with_score_lst = [(self.eval(child), child) for child in children]
        children_with_score_lst.sort(key=lambda x: x[0], reverse=True)
        children = [child for _, child in children_with_score_lst]

        return children

    def get_grid_rival_children(self, grid: Grid):
        children = []
        for cell in grid.getAvailableCells():
            tmp_grid = grid.clone()
            tmp_grid.insertTile(cell, 2)
            children.append(tmp_grid)
            tmp_grid = grid.clone()
            tmp_grid.insertTile(cell, 4)
            children.append(tmp_grid)
        return children

    def evalFun(self, grid):
        maxTile = grid.getMaxTile()
        availableCells = len(grid.getAvailableCells())

        # Path Search Heuristic
        r = 0.5
        score1 = grid.map[3][0] + grid.map[3][1] * r
        score1 += grid.map[3][2] * r ** 2 + grid.map[3][3] * r ** 3
        score1 += grid.map[2][3] * r ** 4 + grid.map[2][2] * r ** 5 + grid.map[2][1] * r ** 6
        score1 += grid.map[2][0] * r ** 7 + grid.map[1][0] * r ** 8 + grid.map[1][1] * r ** 9
        score1 += grid.map[1][2] * r ** 10 + grid.map[1][3] * r ** 11 + grid.map[0][3] * r ** 12
        score1 += grid.map[0][2] * r ** 13 + grid.map[0][1] * r ** 14 + grid.map[0][0] * r ** 15

        score2 = grid.map[3][0] + grid.map[2][0] * r
        score2 += grid.map[1][0] * r ** 2 + grid.map[0][0] * r ** 3
        score2 += grid.map[0][1] * r ** 4 + grid.map[1][1] * r ** 5 + grid.map[2][1] * r ** 6
        score2 += grid.map[3][1] * r ** 7 + grid.map[3][2] * r ** 8 + grid.map[2][2] * r ** 9
        score2 += grid.map[1][2] * r ** 10 + grid.map[0][2] * r ** 11 + grid.map[0][3] * r ** 12
        score2 += grid.map[1][3] * r ** 13 + grid.map[2][3] * r ** 14 + grid.map[3][3] * r ** 15
        return maxTile + 10 * max(score1, score2) + 3 * availableCells
